
export function getTopFivePredictionSeries(topFiveResponse) {

    var predictionArray = []

    let productArray = [
        {
            code: '84077',
            name: 'WORLD WAR 2 GLIDERS ASSTD DESIGNS'
        },
        {
            code: '85099B',
            name: 'JUMBO BAG RED RETROSPOT'
        },
        {
            code: '84879',
            name: 'ASSORTED COLOUR BIRD ORNAMENT'
        },
        {
            code: '85123A',
            name: 'CREAM HANGING HEART T-LIGHT HOLDER'
        },
        {
            code: '21212',
            name: 'PACK OF 72 RETROSPOT CAKE CASES'
        },
    ]

    for (let index = 0; index < topFiveResponse.length; index++) {
        const element = topFiveResponse[index];

        let forecastData = prepareForecastData(element);

        var seriesData = {
            code: productArray[index].code,
            product: productArray[index].name,
            week1: forecastData.week1,
            week2: forecastData.week2,
            total: forecastData.total,
            inventory: forecastData.inventory
        }
        
        predictionArray.push(seriesData);
    }

    return predictionArray;
}

function prepareForecastData(forecast) {

    let week1 = Math.round(forecast[0] + forecast[1] + forecast[2] + forecast[3] + forecast[4] + forecast[5] + forecast[6]);
    let week2 = Math.round(forecast[7] + forecast[8] + forecast[9] + forecast[10] + forecast[11] + forecast[12] + forecast[13]);
    let total = week1 + week2;
    let inventory = getRndInteger(total*0.8, total*1.2);

    return {week1:week1, week2:week2, total:total, inventory:inventory}

}

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}