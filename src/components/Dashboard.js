import React from 'react';
import { View, Text, Button, ScrollView, ActivityIndicator, StyleSheet, Image, Alert } from 'react-native';
import TopFiveChart from './TopFiveChart';
import topFiveRequest from '../../rsc/topfiverequest.json';
import topFiveResponse from '../../rsc/topfiveresponse.json';
import topFiveResponseHelper, { getTopFivePredictionSeries } from '../utilities/topFiveResponseHelper';
import { Table, Row, Rows, TableWrapper, Cell } from 'react-native-table-component';

class Dashboard extends React.Component {

    static navigationOptions = {
        // headerTitle instead of title
        headerTitle: 
        <View style={{flex: 1, flexDirection: 'row', alignContent: 'stretch', justifyContent: 'center'}}>
            <Image source={require('../../rsc/digitelligenceLogo.png')} style={{ width: 40, height: 40 }}/>
        </View>
      };

    constructor() {
        super()
        this.state = {
            isLoading: true,
            isError: false
        }
    }

    componentDidMount() {

        let body = '"' + JSON.stringify(topFiveRequest).replace(/\"/g, '\\\"') + '"'

        fetch('https://0jkxjvour5.execute-api.us-east-2.amazonaws.com/test/digicast', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: body
        }).then((response) => response.json())
            .then((responseJson) => {

                if (responseJson.hasOwnProperty('errorMessage')) {
                    console.log("Error Message " + responseJson.errorMessage);
                    this.setState({
                        isLoading: false,
                        isError: true
                    })
                }
                else {

                    var predicationArray = [];

                    let predicationJson = responseJson;

                    for (let index = 0; index < predicationJson.predictions.length; index++) {
                        predicationArray.push(predicationJson.predictions[index].quantiles["0.9"])
                    }

                    let series = getTopFivePredictionSeries(predicationArray)

                    let tableHeader = ["Code", "Product Name", "Forecast", "Inventory", "Status"]
                    var tableData = []

                    series.forEach(product => {
                        var row = []
                        row.push(product.code)
                        row.push(product.product)
                        row.push(product.total)
                        row.push(product.inventory)
                        row.push(product.inventory - product.total)

                        tableData.push(row)
                    });

                    this.setState({
                        isLoading: false,
                        predicationResponse: series,
                        tableHeader: tableHeader,
                        tableData: tableData
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
    handlePress() {
        console.log('Launching scanner')

    }
    render() {

        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center', padding: 20 }}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
        else {
            if (this.state.isError)
            {
                Alert.alert(
                    'Error',
                    'Please ensure the Sagemaker Endpoint is up and running.',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )

                return <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'flex-start', padding: 5, backgroundColor: '#ffffff' }}></View>
            } 

            return (
                <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'flex-start', padding: 5, backgroundColor: '#ffffff' }}>

                    <ScrollView>
                        <View style={{padding: 7}}>
                            <Text style={{fontSize: 18, textAlign: 'center' }}>Biweekly Sales Forecast</Text>
                        </View>
                        <TopFiveChart series={this.state.predicationResponse} />
                        <View style={{padding: 7}}>
                            <Text style={{fontSize: 18, textAlign: 'center' }}>Inventory Analysis</Text>
                        </View>
                        <Table borderStyle={{ borderWidth: 2, borderColor: '#c8e1ff' }}>
                            <Row data={this.state.tableHeader} flexArr={[1, 2, 1, 1, 1]} style={{ height: 40, backgroundColor: '#f1f8ff' }} textStyle={{ margin: 6, fontSize: 12 }} />
                            {/* <Rows data={this.state.tableData}  flexArr={[1, 2, 1, 1, 1]} style={{ height: 40, backgroundColor: '#ffffff' }} textStyle={{ margin: 6, fontSize: 11 }} /> */
                            
                                this.state.tableData.map((rowData, index) => (
                                <TableWrapper key={index} style={{ flexDirection: 'row' }}>
                                {
                                    rowData.map((cellData, cellIndex) => (
                                    <Cell key={cellIndex} data={cellData} flex={ cellIndex === 1 ? 2:1} textStyle={ cellIndex === 4 ? (cellData > 0 ? { margin: 6, color: '#009f00', fontSize: 11}:{ margin: 6, color: '#df0000',fontSize: 11 }): { margin: 6, fontSize: 11 }}/>
                                ))
                                }
                                </TableWrapper>
                                ))
                            }
                        </Table>
                    </ScrollView>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    head: { height: 40, backgroundColor: '#f1f8ff' },
    text: { margin: 6 }
  });

export default Dashboard;