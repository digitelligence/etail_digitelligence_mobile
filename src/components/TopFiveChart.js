import React, {Component} from 'react';
import ChartView from 'react-native-highcharts';

export default class TopFiveChart extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            series: this.props.series
        }
    }

    render () {

        var categories = [];
        var week1 = [], week2 = [];
        for (let index = 0; index < this.state.series.length; index++) {
            categories.push(this.state.series[index].product)
            week1.push(this.state.series[index].week1)
            week2.push(this.state.series[index].week2)
        }

        var predictionChart = {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top 5 Products'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Number of Products'
                }
            },
            series: [{
                name: 'Week 1',
                data: week1,
                color: '#9F0F00'
            }, {
                name: 'Week 2',
                data: week2, 
                color: '#388A34'
            }],
            exporting: {
                enabled: false
            }
        };

        const options = {
            global: {
                useUTC: false
            },
            lang: {
                decimalPoint: ',',
                thousandsSep: '.'
            }
        };

        return (
            <ChartView style={{ height: 400, padding: 3 }} config={predictionChart} options={options} originWhitelist={['']}></ChartView>
        );
    }

}