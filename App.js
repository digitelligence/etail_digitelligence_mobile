/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { createStackNavigator } from 'react-navigation';
import Scanner from './src/components/Scanner';
import Dashboard from './src/components/Dashboard';
import MyChart from './src/components/MyChart';

export default createStackNavigator(
    {
        Dashboard: Dashboard,
        Scanner: Scanner,
        Chart: MyChart,
    },
    {
        initialRouteName: 'Dashboard',
    }
);
